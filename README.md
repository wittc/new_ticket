```
Usage: ./ticket [command] [options]

Available commands:
    h|help           show this help
    l|ls|list        list existing tickets
    n|new|c|create   create new ticket

Available options:
    List existing commands
        -m|--my                show only ticket where the current user the responsible
	-s|--status=<status>   only show tickets which have this specific state
	-q|--short             only show ticket numbers

    Creating new ticket
        every text/argument passed after the new command are used as title for the new ticket

Examples:
    ./ticket n Script misses a ; at line 27
    ./ticket list --my --status open
```
